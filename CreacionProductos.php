<?php

/*
Utilizo el patron Factory para crear diferentes productos utilizando la misma interfaz
*/

//Interfaz para creacion de productos
interface CreacionProductos 
{
    public function crearProducto();
}

//Implementacion de la interfaz para crear los diferentes productos
class CreadorTelefono implements CreacionProductos
{
    public function crearProducto()
    {
        return new Telefono();
    }
}

class CreadorTelevision implements CreacionProductos
{
    public function crearProducto()
    {
        return new Television();
    }
}

class CreadorLaptop implements CreacionProductos
{
    public function crearProducto()
    {
        return new Laptop();
    }
}

//Interfaz para los productos
interface Product 
{
    public function mostrarDetalles();
}

//Implementacion de interfaz para mostrar detalles
class Telefono implements Product 
{
    public function mostrarDetalles() 
    {
        return "Telefono Celular";
    }
}

class Television implements Product 
{
    public function mostrarDetalles() 
    {
        return "Television";
    }
}

class Laptop implements Product 
{
    public function mostrarDetalles() 
    {
        return "Laptop HP i5";
    }
}

// Cliente que utiliza el Factory para crear y mostrar los productos
class Cliente 
{
    private $creador;

    public function __construct(CreacionProductos $creador) 
    {
        $this->creador = $creador;
    }

    public function CrearYMostrarProducto() 
    {
        $producto = $this->creador->crearProducto();
        echo "Producto creado:" . $producto->mostrarDetalles();
    }
}