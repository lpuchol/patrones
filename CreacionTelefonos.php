<?php
//Utilizo el patron Abstract Factoty para crear televisores (objetos) con caracteristicas similares

// Definicion de la interfaz para productos de 32 pulgadas
interface AbstractPulgadas32
{
    public function estado(): string;
}

// Implementaciones concretas para televisores 32 pulgadas
class SamsungTVPulgadas32 implements AbstractPulgadas32
{
    public function estado(): string
    {
        return "Televisor Samsung 32 pulgadas";
    }
}

class LGTVPulgadas32 implements AbstractPulgadas32
{
    public function estado(): string
    {
        return "Televisor LG 32 pulgadas";
    }
}

//--------------------------------------------------------------

// Definicion de la interfaz para productos de 42 pulgadas
interface AbstractPulgadas42
{
    public function estado(): string;
}

// Implementaciones concretas para televisores 42 pulgadas
class SamsungTVPulgadas42 implements AbstractPulgadas42
{
    public function estado(): string
    {
        return "Televisor Samsung 42 pulgadas";
    }
}

class LGTVPulgadas42 implements AbstractPulgadas42
{
    public function estado(): string
    {
        return "Televisor LG 42 pulgadas";
    }
}

//----------------------------------------------------------------


// Interfaz para la fabrica abstracta que crea productos de 32 y 42 pulgadas
interface AbstractFactory
{
    public function createPulgadas32(): AbstractPulgadas32;
    public function createPulgadas42(): AbstractPulgadas42;
}

// Implementación concreta de la fábrica para televisores Samsung
class SamsungTVFactory implements AbstractFactory
{
    public function createPulgadas32(): AbstractPulgadas32
    {
        return new SamsungTVPulgadas32();
    }
    public function createPulgadas42(): AbstractPulgadas42
    {
        return new SamsungTVPulgadas42();
    }
}

// Implementación concreta de la fábrica para televisores LG
class LGTVFactory implements AbstractFactory
{
    public function createPulgadas32(): AbstractPulgadas32
    {
        return new LGTVPulgadas32();
    }
    public function createPulgadas42(): AbstractPulgadas42
    {
        return new LGTVPulgadas42();
    }
}

?>