<?php
/*
Utilizo el patron COMMAND para enviar mail sobre una nueva compra o una actualizacion
*/
// Interfaz Command
interface Command 
{
    public function execute();
}

// Clase Receptora (Receiver)
class Compra 
{
    public function NuevaCompra() 
    {
        echo "Se ha generado una nueva compra";
    }

    public function ActualizarCompra() 
    {
        echo "Se ha actualizado el estado de su compra";
    }
}

// Comandos concretos
class NuevaCompraCommand implements Command 
{
    private $compra;

    public function __construct(Compra $compra) 
    {
        $this->compra = $compra;
    }

    public function execute() 
    {
        $this->compra->NuevaCompra();
    }
}

class ActualizarCompraCommand implements Command
{
    private $compra;

    public function __construct(Compra $compra) 
    {
        $this->compra = $compra;
    }

    public function execute() 
    {
        $this->compra->ActualizarCompra();
    }
}

// Invocador (Invoker)
class Remitente 
{
    private $command;

    public function setCommand(Command $command) 
    {
        $this->command = $command;
    }

    public function enviarMail() 
    {
        $this->command->execute();
    }
}