<?php
/*
Utilizo el patron DECORATOR para agregarle un descuento o la opcion de envio rapidop
*/
// Interfaz para los productos
interface Producto
{
    public function obtenerDescripcion(): string;
    public function obtenerPrecio(): float;
}

// Implementación concreta de Producto
class ProductoElectronico implements Producto
{
    private $descripcion;
    private $precio;

    public function __construct(string $descripcion, float $precio)
    {
        $this->descripcion = $descripcion;
        $this->precio = $precio;
    }

    public function obtenerDescripcion(): string
    {
        return $this->descripcion;
    }

    public function obtenerPrecio(): float
    {
        return $this->precio;
    }
}

// Clase base para los decoradores
class DecoradorProducto implements Producto
{
    protected $producto;

    public function __construct(Producto $producto)
    {
        $this->producto = $producto;
    }

    public function obtenerDescripcion(): string
    {
        return $this->producto->obtenerDescripcion();
    }

    public function obtenerPrecio(): float
    {
        return $this->producto->obtenerPrecio();
    }
}

// Decorador que agrega la funcionalidad de envio rápido
class DecoradorEnvioRapido extends DecoradorProducto
{
    public function obtenerDescripcion(): string
    {
        return parent::obtenerDescripcion() . " + Envío Rápido";
    }

    public function obtenerPrecio(): float
    {
        // Costo adicional por envío rápido
        return parent::obtenerPrecio() + 5.0;
    }
}

// Decorador concreto que agrega la funcionalidad de descuento
class DecoradorDescuento extends DecoradorProducto
{
    private $porcentajeDescuento;

    public function __construct(Producto $producto, float $porcentajeDescuento)
    {
        parent::__construct($producto);
        $this->porcentajeDescuento = $porcentajeDescuento;
    }

    public function obtenerDescripcion(): string
    {
        return parent::obtenerDescripcion() . " - Descuento {$this->porcentajeDescuento}%";
    }

    public function obtenerPrecio(): float
    {
        // Aplico el descuento al precio original.
        return parent::obtenerPrecio() * (1 - $this->porcentajeDescuento / 100);
    }
}
