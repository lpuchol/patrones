<?php     

//Utilizo Singleton para crear, ver y cerrar la sesion iniada

class UserSession
{
    private static $instance;
    private $userName;

    //Constructor
    private function __construct() {}
    // Evita la clonación de la instancia
    private function __clone() {}

    // Evita la serialización de la instancia
    private function __wakeup() {}

    //Manejo de instancia
    public static function getInstance(): UserSession
    {
        if (!isset(self::$instance)) 
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //Asigar username a la session
    public function asignarUsername(string $user)
    {
        $this->userName = $user;
    }

    //Obtener username de la session
    public function obtenerUsername(): ?string
    {
        return $this->userName ?? null;
    }

    //Desloguear session
    public function logout()
    {
        // Limpiar la variable de sesión al cerrar sesión
        unset($this->userName);
    }
    
    //Verificar si se ha iniciado sesión
    public function isLoggedIn(): bool
    {
        
        return isset($this->userName);
    }
}

?>