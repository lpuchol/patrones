<?
/*
Utilizo el patron ADAPTER para mostrar los diferentes productos
*/
// Clase para telefono
class TelefonoMovil 
{
    private $modelo;

    public function __construct($modelo) 
    {
        $this->modelo = $modelo;
    }

    public function getModelo() 
    {
        return $this->modelo;
    }
}

// Clase para televisores
class Televisor 
{
    private $marca;

    public function __construct($marca) 
    {
        $this->marca = $marca;
    }

    public function getMarca() 
    {
        return $this->marca;
    }
}

// Clase para PCs
class PC 
{
    private $marca;

    public function __construct($marca) 
    {
        $this->marca = $marca;
    }

    public function getMarca() 
    {
        return $this->marca;
    }
}

// Interfaz común para productos
interface ProductoInterfaz 
{
    public function obtenerDescripcion();
}

// Adaptadores para telefonos moviles, televisores y PCs
class TelefonoMovilAdapter implements ProductoInterfaz 
{
    private $telefonoMovil;

    public function __construct(TelefonoMovil $telefonoMovil) 
    {
        $this->telefonoMovil = $telefonoMovil;
    }

    public function obtenerDescripcion() 
    {
        return "Teléfono Móvil: " . $this->telefonoMovil->getModelo();
    }
}

class TelevisorAdapter implements ProductoInterfaz 
{
    private $televisor;

    public function __construct(Televisor $televisor) 
    {
        $this->televisor = $televisor;
    }

    public function obtenerDescripcion() 
    {
        return "Televisor: " . $this->televisor->getMarca();
    }
}

class PCAdapter implements ProductoInterfaz
{
    private $pc;

    public function __construct(PC $pc) 
    {
        $this->pc = $pc;
    }

    public function obtenerDescripcion() 
    {
        return "PC: " . $this->pc->getMarca();
    }
}