<?php
/*
Utilizo el patron OBSERVER para notificar la actualizacion de stock
*/
// Definición de la interfaz Subject
interface Subject 
{
    public function attach(Observer $observer); // Metodo para adjuntar un observador
    public function detach(Observer $observer); // Metodo para desadjuntar un observador
    public function notificar(); //Metodo para notificar a todos los observadores
}

// Definicion de la interfaz Observer
interface Observer 
{
    public function update($nuevoProducto);
}

// Clase que actua como el Sujeto concreto
class ProductNotifier implements Subject 
{
    private $observers = [];
    private $nuevoProducto;

    //Adjuntar un observador a la lista
    public function attach(Observer $observer) 
    {
        $this->observers[] = $observer;
    }

    //Desadjuntar un observador de la lista
    public function detach(Observer $observer) 
    {
        $index = array_search($observer, $this->observers);
        if ($index !== false) 
        {
            unset($this->observers[$index]);
        }
    }

    //Agregar Producto
    public function AgregarProducto($productName) 
    {
        $this->nuevoProducto = $productName;
        $this->notificar();
    }

    //Obtener Producto
    public function ObtenerNuevoProducto() 
    {
        return $this->nuevoProducto;
    }

    //Notifacacion
    public function notificar() 
    {
        foreach ($this->observers as $observer) 
        {
            $observer->update($this->nuevoProducto);
        }
    }
}

//Clase suscriptor de correo electrónico
class SuscriptorCorreo implements Observer 
{
    private $email;

    public function __construct($email) 
    {
        $this->email = $email;
    }

    public function update($nuevoProducto) 
    {
        $this->EnviarEmail($nuevoProducto);
    }

    private function EnviarEmail($nuevoProducto) 
    {
        $subject = "Nuevo producto disponible";
        $message = "¡Lanzamos un nuevo producto: $nuevoProducto! Visita nuestro sitio para obtener más detalles.";

        // Aquí iría la lógica real para enviar un correo electrónico
        echo "Enviando correo a $this->email";
    }
}


