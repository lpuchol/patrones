<?php
/*
Utilizo el patron STRATEGY para seleccionar el tipo de pago, tarjeta o transferencia
*/

interface EstrategiaDePago
{
    public function procesarPago(float $amount): string;
}

class CreditCardPayment implements EstrategiaDePago
{
    public function procesarPago(float $amount): string
    {
        return "Pago con tarjeta de crédito procesado";
    }
}

class BankTransferPayment implements EstrategiaDePago
{
    public function procesarPago(float $amount): string
    {
        return "Pago con transferencia bancaria procesado";
    }
}

class PaymentContext
{
    private $estrategiaDePago;

    public function __construct(EstrategiaDePago $estrategiaDePago)
    {
        $this->estrategiaDePago = $estrategiaDePago;
    }

    public function setEstrategiaDePago(EstrategiaDePago $estrategiaDePago)
    {
        $this->estrategiaDePago = $estrategiaDePago;
    }

    public function procesarPago(float $amount): string
    {
        return $this->estrategiaDePago->procesarPago($amount);
    }
}
