<?php
class carrito
{
    private $productos = [];

    public function add_product($producto)
    {
        $this->productos[] = $producto;

        if (count($this->get_producto()) === 0) 
        {
            throw new \Exception("El producto no se agrego correctamente");
        }
    }

    public function get_producto()
    {
        return $this->productos;
    }
}

function can_add_producto()
{
    $carrito = new Carrito();

    // Agregar un producto al carrito
    $carrito->add_product("Producto de ejemplo");

    $this->assertEquals(["Producto de ejemplo"], $carrito->get_producto());
}

?>